Covid19DataBE

This tool:

1. Downloads the covid19 cases data for Belgium (.csv file)
(file - import can be used for your own analysis in spreadsheet)

2. Makes a list of the municipalities available, as csv file

3. Filters the data on the choosen municipality to .csv file
(this filtered data can be imported in spreadsheet to make graphs)

4. draws a x-axis time y-axis cases graph of the filtered data 

