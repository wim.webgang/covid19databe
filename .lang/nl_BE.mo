��    -      �  =   �      �     �  :   �     #     )     0  :   N  <   �     �     �     �  I   �     E  5   J     �      �     �  ,   �       �                       $     *  A   C     �     �     �  $   �     �     �  ,   �            �   6     -	      @	  &   a	     �	     �	     �	     �	     �	  	   �	  �   �	     �
  <   �
     �
     �
     �
  ?     A   U     �     �     �  4   �     �  ,   �          8     X  +   u     �    �     �     �     �     �     �  =   �  	   "     ,  
   3      >     _     |  2   �     �     �    �     �     �  '        ;     ?     E     U     b     j     -   ,                                                          #      *   (             	       "      +   &                   !   $                              
   )                    '       %                  today Autorun to filter and show graph automatically from now on Cases Choose Choose municipality (by name) Choosen NIS code will be used to filter municipality data  Choosen province will be used to filter hospitalisation data Close the application Covid19DataBE Create directory Create the directory as proposed here, then save it as selected directory Data Data processed: (filtered columns and municipality..) Data to process: (dowloaded ..) Download dataset of previous day Download dataset of today Download latest datasets from Sciensano site Filter cases Filter data from Belgian Sciensano csv dataset about COVID-19 Belgium
Epidemiological Situation, Municipality Monitoring. 
Day to day follow up of choosen locality is not present in the Sciensano interface (at the start of this project june 2020) Filter hosp. Graph Help Hosp. NIS5 code (manual entry) Please set a directory to store the downloads and the result data Quit Save Save directory Selected Province (choose from list) Selected location to filter on Set Settings City or Province (choose from list) Show hidden Show window with graph You seem to have started this program for the first time (because there are no settings saved); we will have to check this first:
Please set a directory to store the downloads and the result data.
Or go create one and come back here to choose it. cases municipality choose NIS5 code of municipality choose province (for hospitalisations) day day-1 hospitalisations some tips for use.. today yesterday Project-Id-Version: covid19databe 3.9.2
PO-Revision-Date: 2020-10-30 08:29 UTC
Last-Translator: wim <wim@Probook2A.ara>
Language: nl_BE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 vandaag Automatisch filteren en grafiek tonen vanaf de volgende keer Gevallen Kies Kies gemeente (op naam) Gekozen nis5 code wordt gebruikt om de gemeente uit te filteren Gekozen provincie wordt gebruikt om te filteren op hospitalisatie Verlaat het programma - Maak map Maak de map met deze naam.  (Druk daarna op bewaren) - Verwerkt (kolommen en gemeente uitgefilterd) Te verwerken (afgehaald...) Haal gegevens van vorige dag af Haal gegevens van vandaag af Haal laatste gegevens van Sciensano website Filter gevallen Filter gegeven van Belgische Sciensano csv-dataset over COVID-19 Belgische 
Epidemilogische situtatie, Gemeenteopvolging.  
Dagelijkse opvolging van een gekozen gemeente is niet voorzien in de Sciensano website (op het moment dat dit project werd gestart in juni 2020). - Grafiek Help - NIS5 code (ingeven) Kies een map om de downloads te bewaren, alsook de resultaten Afsluiten Bewaar Bewaar map Gekozen provincie (uit de lijst) Plaatsnaam om op te filteren Stel in Instelling Gemeente of Provincie (keuze uit lijst) Toon verborgen Toon venster met grafiek Het ziet er naar uit dat je dit programma voor het eerst draait (omdat er nog geen instellingen bewaard zijn) ; we kijken die eerst na. 
 
Kies een map om de downloads te bewaren; alsook de resultaten. 
 
Of maak een nieuwe map aan en kom terug om ze te kiezen. gevallen gemeente Kies NIS5 code van gemeente gekozen provincie (voor hospitalisatie) dag dag-1 hospitalisaties aanwijzingen vandaag gisteren 